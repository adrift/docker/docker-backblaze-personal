echo "Starting the virtual display & vnc server"
rm -f /tmp/.X0-lock
Xvfb :0 -screen 0 1024x768x24 & openbox & x11vnc -nopw -q -forever -loop -shared &

function configure_wine64 {
	mkdir -p "$WINEPREFIX"
	unlink $WINEPREFIX/dosdevices/z:
	ln -s /data/ $WINEPREFIX/dosdevices/d:

	if [ ${#COMPUTER_NAME} -gt 15 ]; then 
		echo "Error: computer name cannot be longer than 15 characters"
		exit 1
	fi
	echo "Setting the wine64 computer name"
	wine64 reg add "HKCU\\SOFTWARE\\Wine\\Network\\" /v UseDnsComputerName /f /d N
	wine64 reg add "HKLM\\SYSTEM\\CurrentControlSet\\Control\\ComputerName\\ComputerName" /v ComputerName /f /d $COMPUTER_NAME
	wine64 reg add "HKLM\\SYSTEM\\CurrentControlSet\\Control\\ComputerName\\ActiveComputerName" /v ComputerName /f /d $COMPUTER_NAME
}

if [ -f "$WINEPREFIX/drive_c/Program\ Files/Backblaze/bzbui.exe.aside" ]; then
    cp $WINEPREFIX/drive_c/Program\ Files/Backblaze/bzbui.exe.aside $WINEPREFIX/drive_c/Program\ Files/Backblaze/bzbui.exe
fi

if [ ! -f "$WINEPREFIX/drive_c/Program\ Files/Backblaze/bzbui.exe" ]; then
	echo "Backblaze not installed"
	echo "Initializing the wine64 prefix"
	wine64 wineboot -i -u
	configure_wine64
	echo "Downloading the Backblaze personal installer..."
	wget https://www.backblaze.com/win32/install_backblaze.exe -P $WINEPREFIX/drive_c/
	echo "Backblaze installer started, please go through the graphical setup in by logging onto the containers vnc server"
	wine64 $WINEPREFIX/drive_c/install_backblaze.exe
	echo "Installation finished or aborted, trying to start the Backblaze client..."
fi

wineserver -k
configure_wine64
echo "Backblaze found, starting the Backblaze client..."
wine64 $WINEPREFIX/drive_c/Program\ Files/Backblaze/bzbui.exe -noqiet

while true; do sleep 60; done
